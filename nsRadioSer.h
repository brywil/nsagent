/*****************************************************************************
 * Project			: nsAgent
 * Author			: Bryan Wilcutt bwilcutt@yahoo.com
 * Date				: 5-13-18
 * System			: Nano PI
 * File				: nsRadioSer.h
 *
 * Description		:
 *
 * This file contains support constructs for nsRadioSer.c.
 *
 * Written for Novus Power.
 *
 * Copyright (c) Novus Power All Rights Reserved
 *****************************************************************************/
#ifndef NSRADIOSER_H_
#define NSRADIOSER_H_

extern int radioIFStart();


#endif /* NSRADIOSER_H_ */
